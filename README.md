José
=========
Welcome to José! José is a multi-function Discord bot made with Python and discord.py.

# Requirements

- (Preferrably) A Linux system
- Python 3
- PostgreSQL

# Installation

```bash
git clone https://gitlab.com/luna/jose.git
cd jose

# edit joseconfig.py as you wish
cp example_config.py joseconfig.py

# edit jcoin/config.py as you wish
cp jcoin/config-example.py jcoin/config.py

sudo -u psql

CREATE USER jose WITH PASSWORD 'something_secure';
CREATE DATABASE jose;
GRANT ALL PRIVILEGES ON DATABASE jose TO jose;

<exit psql>

# load tables
psql -U jose -W -f jcoin/schema.sql

# from there install the depedencies in requirements.txt and
# jcoin/requirements.txt as you see fit (in a virtualenv
# or not, etc).
```

# Running

```sh
# run jcoin/josecoin.py BEFORE jose.py
# or you're in for a bad time.

# this section doesnt have any commands intentially
# since you can do anything, be it manually + tmux,
# or pm2, or whatever.

# the pipupdates cog requires a virtual env on the env folder
```

# Help

[Bot OAuth URL](https://discordapp.com/oauth2/authorize?permissions=379968&scope=bot&client_id=202586824013643777)

[Discord server](https://discord.gg/5ASwg4C)
