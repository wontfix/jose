import discord
from discord.ext import commands
from dns import resolver, rdatatype

from .common import Cog, SayException


def _add_dns_field(em, resp):
    resp_dt = resp.rdtype

    if resp_dt in (rdatatype.A, rdatatype.AAAA):
        em.add_field(name="address", value=resp.address)

    if resp_dt == rdatatype.SRV:
        em.add_field(
            name="srv data",
            value=(
                f"port: {resp.port} "
                f"priority: {resp.priority} "
                f"target: {resp.target} "
                f"weight: {resp.weight}"
            ),
        )

    if resp_dt in (rdatatype.CNAME, rdatatype.NS, rdatatype.PTR):
        em.add_field(
            name=rdatatype.to_text(resp_dt),
            value=f"target: {resp.target}",
        )

    if resp_dt == rdatatype.MX:
        em.add_field(
            name="MX",
            value=f"exchange: {resp.exchange} preference: {resp.preference}",
        )

    if resp_dt == rdatatype.TXT:
        em.add_field(name="TXT", value=f"{resp.strings!r}")


class DNS(Cog):
    """DNS resolver."""

    def __init__(self, bot):
        super().__init__(bot)
        self.resolver = resolver.Resolver()

    def _resolve_fmt(self, ctx, inp, res):
        domain, rrtype = inp

        em = discord.Embed(
            title=f"dns results for {domain} {rrtype}", color=discord.Color.green()
        )

        for resp in res:
            _add_dns_field(em, resp)

        return em

    @commands.command(aliases=["dig"])
    async def resolve(self, ctx, domain: str, rrtype: str = "A"):
        """Resolve a domain.

        RR types are defined by the DNS spec.

        By default, it will assume an A RRtype.
        """
        rrtype = rrtype.upper()

        try:
            with ctx.typing():
                res = await self.loop.run_in_executor(
                    None, self.resolver.query, domain, rrtype
                )

                em = self._resolve_fmt(ctx, (domain, rrtype), res)
                await ctx.send(embed=em)
        except resolver.Timeout:
            raise SayException(":x: timed out")
        except resolver.NXDOMAIN:
            raise SayException(":x: NXDOMAIN (domain not found)")
        except resolver.NoAnswer:
            raise SayException(":x: resolver did not answer")


def setup(bot):
    bot.add_jose_cog(DNS)
